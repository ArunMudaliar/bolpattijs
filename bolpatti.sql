-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2016 at 03:43 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bolpatti`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `g_id` int(30) NOT NULL,
  `u_id` int(10) NOT NULL,
  `g_status` char(3) NOT NULL DEFAULT 'ON',
  `g_dt` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`g_id`, `u_id`, `g_status`, `g_dt`) VALUES
(1, 1, 'ON', '1456261119059'),
(2, 1, 'ON', '1456261232786');

-- --------------------------------------------------------

--
-- Table structure for table `game_table`
--

CREATE TABLE `game_table` (
  `t_id` int(30) NOT NULL,
  `g_id` int(30) NOT NULL,
  `table_name` varchar(10) DEFAULT NULL,
  `is_running` varchar(1) NOT NULL DEFAULT 'Y',
  `is_active` varchar(1) NOT NULL DEFAULT 'Y',
  `g_dt` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_table`
--

INSERT INTO `game_table` (`t_id`, `g_id`, `table_name`, `is_running`, `is_active`, `g_dt`) VALUES
(1, 1, '1ybbSs', 'Y', 'Y', '1456261119064'),
(2, 2, '2AJMYG', 'Y', 'Y', '1456261232792');

-- --------------------------------------------------------

--
-- Table structure for table `game_table_detail`
--

CREATE TABLE `game_table_detail` (
  `g_t_d_id` int(30) NOT NULL,
  `g_id` int(30) NOT NULL,
  `t_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `position` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `i_date` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_table_detail`
--

INSERT INTO `game_table_detail` (`g_t_d_id`, `g_id`, `t_id`, `u_id`, `position`, `status`, `i_date`) VALUES
(1, 1, 1, 1, 0, 1, '1456261119068'),
(2, 2, 2, 1, 0, 1, '1456261232800');

-- --------------------------------------------------------

--
-- Table structure for table `gift`
--

CREATE TABLE `gift` (
  `gift_id` int(30) NOT NULL,
  `gift_category` varchar(255) NOT NULL,
  `gift_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gift_details`
--

CREATE TABLE `gift_details` (
  `gift_d_id` int(30) NOT NULL,
  `gift_id` int(30) NOT NULL,
  `gift_d_name` varchar(255) NOT NULL,
  `gift_d_coins` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `round`
--

CREATE TABLE `round` (
  `r_id` int(30) NOT NULL,
  `t_id` int(30) NOT NULL,
  `u_won_id` int(30) DEFAULT NULL,
  `card_sequence` varchar(255) DEFAULT NULL,
  `selected_card_index` varchar(4) DEFAULT NULL,
  `lead_u_id` int(30) DEFAULT NULL,
  `win_cardseq_index` varchar(15) DEFAULT NULL,
  `win_index` int(1) DEFAULT '0',
  `is_active` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `round`
--

INSERT INTO `round` (`r_id`, `t_id`, `u_won_id`, `card_sequence`, `selected_card_index`, `lead_u_id`, `win_cardseq_index`, `win_index`, `is_active`) VALUES
(1, 1, NULL, '"[{\\"index\\":42,\\"value\\":11,\\"name\\":\\"J\\",\\"suit\\":\\"Clubs\\"},{\\"index\\":31,\\"value\\":8,\\"name\\":\\"8\\",\\"suit\\":\\"Hearts\\"},{\\"index\\":15,\\"value\\":4,\\"name\\":\\"4\\",\\"suit\\":\\"Hearts\\"},{\\"index\\":32,\\"value\\":8,\\"name\\":\\"8\\",\\"suit\\":\\"Spades\\"},{\\"in', '1', 1, '20,23,31,48', NULL, '0'),
(2, 2, NULL, '"[{\\"index\\":50,\\"value\\":13,\\"name\\":\\"K\\",\\"suit\\":\\"Clubs\\"},{\\"index\\":34,\\"value\\":9,\\"name\\":\\"9\\",\\"suit\\":\\"Clubs\\"},{\\"index\\":27,\\"value\\":7,\\"name\\":\\"7\\",\\"suit\\":\\"Hearts\\"},{\\"index\\":14,\\"value\\":4,\\"name\\":\\"4\\",\\"suit\\":\\"Clubs\\"},{\\"inde', '1', 1, '23,43,44,50', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `round_details`
--

CREATE TABLE `round_details` (
  `rd_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `r_id` int(30) NOT NULL,
  `bid_coin` int(4) NOT NULL,
  `bid_x` int(1) NOT NULL,
  `total_bid` bigint(100) NOT NULL,
  `in_out` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `round_details`
--

INSERT INTO `round_details` (`rd_id`, `u_id`, `r_id`, `bid_coin`, `bid_x`, `total_bid`, `in_out`) VALUES
(1, 1, 1, 500, 1, 500, 0),
(2, 1, 1, 500, 1, 500, 0),
(3, 1, 1, 500, 1, 500, 0),
(4, 1, 1, 500, 1, 500, 0),
(5, 1, 1, 500, 1, 500, 0),
(6, 1, 2, 100, 1, 100, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(30) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(50) DEFAULT NULL,
  `u_gender` varchar(6) DEFAULT NULL,
  `fb_id` varchar(50) DEFAULT NULL,
  `fb_mobile` varchar(50) DEFAULT NULL,
  `is_guest` varchar(4) DEFAULT NULL,
  `pushtoken` varchar(255) DEFAULT NULL,
  `deviec_type` varchar(10) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `balance` bigint(100) NOT NULL DEFAULT '10000',
  `reg_dt` varchar(40) NOT NULL,
  `last_login` varchar(40) DEFAULT NULL,
  `chk_last_login` varchar(40) DEFAULT NULL,
  `is_loggedIn` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_name`, `u_email`, `u_gender`, `fb_id`, `fb_mobile`, `is_guest`, `pushtoken`, `deviec_type`, `token`, `balance`, `reg_dt`, `last_login`, `chk_last_login`, `is_loggedIn`) VALUES
(1, 'gggg', '', '0', '', '9898989898', '1', '5465464', 'Android', '353627071660032', 10000, '1456261117276', '1456261230367', '1456261117276', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_purchase`
--

CREATE TABLE `user_purchase` (
  `up_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `u_pur_token` varchar(255) NOT NULL,
  `u_dt` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`g_id`);

--
-- Indexes for table `game_table`
--
ALTER TABLE `game_table`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `game_table_detail`
--
ALTER TABLE `game_table_detail`
  ADD PRIMARY KEY (`g_t_d_id`);

--
-- Indexes for table `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`gift_id`);

--
-- Indexes for table `gift_details`
--
ALTER TABLE `gift_details`
  ADD PRIMARY KEY (`gift_d_id`);

--
-- Indexes for table `round`
--
ALTER TABLE `round`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `round_details`
--
ALTER TABLE `round_details`
  ADD PRIMARY KEY (`rd_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `user_purchase`
--
ALTER TABLE `user_purchase`
  ADD PRIMARY KEY (`up_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `g_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game_table`
--
ALTER TABLE `game_table`
  MODIFY `t_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game_table_detail`
--
ALTER TABLE `game_table_detail`
  MODIFY `g_t_d_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gift`
--
ALTER TABLE `gift`
  MODIFY `gift_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gift_details`
--
ALTER TABLE `gift_details`
  MODIFY `gift_d_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `round`
--
ALTER TABLE `round`
  MODIFY `r_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `round_details`
--
ALTER TABLE `round_details`
  MODIFY `rd_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_purchase`
--
ALTER TABLE `user_purchase`
  MODIFY `up_id` int(30) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
