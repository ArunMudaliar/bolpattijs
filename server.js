// Develope by Nishit Maheta
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var mysql = require('mysql');
var io = require('socket.io').listen(app.listen(port)); // Connect to server
var LruMap = require("collections/lru-map");
var map = new LruMap();
var winCardSeqIndex = new LruMap();	// stores the possible values for the game round winning card sequence

var game_id, game_table_id, game_table_detail_id, round_id, round_detail_id, user_id = 0;
var OUT = 1;
var IN = 0;
var table_name_db = '';
var socketRooms = [];
var shuffle = require('./shuffle'); // Include shuffle module
var bidTimeOut = '20';
// Generate Time stamp
function getTimestamp() {
    process.env.TZ = 'Asia/Kolkata';
    var d = new Date();
    return d.getTime();
    //return (new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''));
}


function getHoursDifference(firstTime, secondTime) {
    var diff = Math.abs(firstTime - secondTime) / 3600000;
    return parseInt(diff);
}


// Generate random table name
function generateTableName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function getUserData(id, userToken) {
    connection.query('SELECT * FROM user where token = ' +"'"+ userToken+"'", function(err, rows) {
        if (err) throw err;

        if (rows.length > 0) {
            io.sockets.in(id).emit('successRegister', {
                "userData": rows
            });
        } else {
            io.sockets.in(id).emit('successRegister', {
                "userData": null
            });
        }
    });
}


// Shuffle Cards
function shuffleCard(table_name) {
    shuffleObj = new shuffle();
    var desk = shuffleObj.deck();
    var cardsArray = shuffleObj.shuffleArray(desk);
    //socket.room = data.table_name;
    map.set(table_name, cardsArray);
    return cardsArray
        /*if(isEmit === true){
		io.sockets.in(data.table_name).emit('successShuffle', { "shuffleCards" :cardsArray });						   
	}else{
		return cardsArray;
	}*/
}

// Mysql database connection
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'bolpatti',
});

connection.connect(function(err) {
    if (err) {
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
});

// Socket connection event 
io.sockets.on('connection', function(socket) {

    console.log('User Connected: ' + socket.id);

    io.sockets.in(socket.id).emit('getId', {
        id: socket.id
    });

    socket.on('disconnect', function() {
        console.log('User disconnected: ' + socket.id);
        //console.log(socket)
    });

    // Register new usr evetn
    socket.on('userLogin', function(data) {

        console.log("new user register request " + JSON.stringify(data));
        console.log("userLogin " + data.token);
        // Add user data to database
        connection.query('SELECT * FROM user where token = ' +"'"+ data.token+"'", function(err, rows) {
            if (err) throw err;
            console.log("userLogin " + rows.length);
            if (rows.length > 0) {
                // Check for users last login to update bonus point
                var getHrs = getHoursDifference(rows[0].chk_last_login, getTimestamp());
                var Query = '';
                var dataArray = [];
                var Bal = parseInt(rows[0].balance);
                if (getHrs >= 24) {
                    Query = "UPDATE user SET chk_last_login = ?,last_login =?,balance=?  Where token = ?";
                    Bal = Bal + 1000;
                    dataArray = [getTimestamp(), getTimestamp(), Bal, data.token];
                    rows[0].balance = Bal;
                } else {
                    Query = "UPDATE user SET last_login =? Where token = ?";
                    dataArray = [getTimestamp(), data.token];
                }

                connection.query(Query, dataArray,
                    function(err, result) {
                        if (err) throw err;

                        console.log("successRegister " + data);
                        io.sockets.in(data.id).emit('successRegister', {
                            "userData": rows[0]
                        });
                    }
                );

            } else {
                connection.query('INSERT INTO user SET ?', {
                    u_name: data.u_name,
                    u_email: data.u_email,
                    fb_id: data.fb_id,
                    u_gender: data.u_gender,
                    fb_mobile: data.fb_mobile,
                    is_guest: data.is_guest,
                    pushtoken: data.pushtoken,
                    deviec_type: data.deviec_type,
                    token: data.token,
                    chk_last_login: getTimestamp(),
                    last_login: getTimestamp(),
                    reg_dt: getTimestamp()
                }, function(err, res) {
                    if (err) throw err;
                    console.log("new user register successffuly " + res.insertId);
                    getUserData(data.id, data.token);
                });
            }
        });

    });


    socket.on("joinTable", function(data) {
        connection.query('SELECT * FROM game_table where t_id = ' + data.table_id, function(err, trows) {
            if (err) throw err;
            if (trows.length > 0) {
                connection.query('SELECT gtd.*, max(gtd.position)+1 maxpos FROM game_table_detail gtd where t_id = ' + data.table_id, function(err, tdrows) {
                    if (err) throw err;
                    if (tdrows.length > 0 && tdrows.maxpos <= 5) { // Check  total users in game . if table is not full add user in game
                        connection.query('INSERT INTO game_table_detail SET ?', {
                            g_id: data.game_id,
                            t_id: data.table_id,
                            u_id: data.u_id,
                            position: tdrows.maxpos,
                            i_date: getTimestamp()
                        }, function(err, tdres) {
                            if (err) throw err;
                            game_table_detail_id = tdres.insertId;
                            socket.room = trows[0].table_name;
                            socket.join(trows[0].table_name);

                            connection.query('SELECT * FROM user where u_id = ' + data.u_id, function(err, rows) {
                                if (err) throw err;
                                if (rows.length > 0) {
                                    rows[0].table_name = trows[0].table_name;
                                    rows[0].game_table_detail_id = game_table_detail_id;
                                    rows[0].bidTimeOut = bidTimeOut;
                                    rows[0].shuffleCards = map.get(data.table_name);
                                    io.sockets.in(trows[0].table_name).emit('successJoinTable', {
                                        "userData": rows
                                    });
                                } else {
                                    io.sockets.in(trows[0].table_name).emit('successJoinTable', {
                                        "userData": []
                                    });
                                }
                            });
                        });
                    } else {
                        // If table is full call room full event with user data
                        /*
                        connection.query('SELECT * FROM user where u_id = ' + data.u_id, function(err, rows) {
                            if (err) throw err;

                            if (rows.length > 0) {
                                io.sockets.in(data.id).emit('room_full', {
                                    "userData": rows
                                });
                            } else {
                                io.sockets.in(data.id).emit('room_full', {
                                    "userData": null
                                });
                            }
                        });
                        */
                        
                        connection.query('INSERT INTO game SET ?', {
                                u_id: data.u_id,
                                g_dt: getTimestamp()
                            }, function(err, res) {
                                if (err) throw err;
                                game_id = res.insertId;
                                table_name_db = game_id + generateTableName();
                                socket.room = table_name_db;
                                socket.join(table_name_db);
                                // Create new game table
                                connection.query('INSERT INTO game_table SET ?', {
                                    g_id: game_id,
                                    table_name: table_name_db,
                                    g_dt: getTimestamp()
                                }, function(err, tres) {
                                    if (err) throw err;
                                    game_table_id = tres.insertId;
                                    // Create game table detail entry 
                                    connection.query('INSERT INTO game_table_detail SET ?', {
                                        g_id: game_id,
                                        t_id: game_table_id,
                                        u_id: data.u_id,
                                        position: 0,
                                        i_date: getTimestamp()
                                    }, function(err, tdres) {
                                        if (err) throw err;
                                        game_table_detail_id = tdres.insertId;
                                        io.sockets.in(data.id).emit('successGame', {
                                            "game_id": game_id,
                                            'table_id': game_table_id,
                                            'table_detail_id': game_table_detail_id,
                                            'table_name': table_name_db,
                                            'bidTimeOut': bidTimeOut,
                                            'position': 0,
                                            'shuffleCards': shuffleCard(table_name_db)
                                        });
                                    });
                                });
                            });
                    }
                });
            }
        });
    });

    function checkOddEven(number) {
        if (number % 2 == 0) {
            return OUT;
        } else {
            return IN;
        }
    }

	function checkWhoWonRound(data){
        //selected_card_index == the index within 1 to 52 that is being hold by me in the game from shuffle time
        //var r_id, t_id, selected_card_index, win_card_seq_index,selected_card_index;
        var bid_type = checkOddEven(data.win_card_seq_index);

        if (bid_type == OUT && data.selected_card_index == data.win_card_seq_index) {
            // user won,
            // make a query fetch bid coins for all user & return them,
            // special case if rows[i].u_id == lead_u_id return double coin
            // save all this in database

            // make a query fetch bid coins for all user & return them,
            connection.query('SELECT * FROM round_details where r_id = ' + data.r_id, function(err, rows) {
                if (err) throw err;
                if (rows.length > 0) {
                    for (i = 0; i < rows.length; i++) {
                        if (rows[i].u_id == data.lead_u_id) {
                            var plusUserBalanceBy = (2(rows[i].bid_x * rows[i].bid_coin)); // user won
                            rows[i].won_coin = plusUserBalanceBy;
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = 0;
                        } else {
                            rows[i].won_coin = 0;
                            rows[i].return_coin = (rows[i].bid_x * rows[i].bid_coin);
                            rows[i].lost_coin = 0;
                        }
                        if ((i + 1) === rows.length) {
                            io.sockets.in(data.table_name).emit('whoWonSuccess', {
                                "data": rows,
								"isRoundEnd":data.isRoundEnd
                            });
                        }
                    }
                } else {
                    io.sockets.in(data.table_name).emit('whoWonSuccess', {
                        "data": [],
						"isRoundEnd":data.isRoundEnd
                    });
                }
            });

        } else {
            connection.query('SELECT * FROM round_details where r_id = ' + data.r_id, function(err, rows) {
                if (err) throw err;

                if (rows.length > 0) {
                    for (i = 0; i < rows.length; i++) {
                        if (bid_type == rows[i].in_out) {
                            // user won
                            rows[i].won_coin = (rows[i].bid_x * rows[i].bid_coin);
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = 0;
                        } else {
                            // user lost
                            rows[i].won_coin = 0;
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = (rows[i].bid_x * rows[i].bid_coin);
                        }

                        if ((i + 1) === rows.length) {
                            io.sockets.in(data.table_name).emit('whoWonSuccess', {
                                "data": rows,
								"isRoundEnd":data.isRoundEnd
                            });
                        }
                    }
                } else {
                    io.sockets.in(data.table_name).emit('whoWonSuccess', {
                        "data": [],
						"isRoundEnd":data.isRoundEnd
                    });
                }
            });
            //io.sockets.in(data.table_name).emit('whoWonSuccess', { "start" : 'yes' });          
        }

    }
    // Start Who Won 
    socket.on('whoWon', function(data) {
        //card_seq_index == the index within 1 to 52 that is being hold by me in the game from shuffle time
        //var r_id, t_id, selected_card_index, win_card_seq_index,card_seq_index;
        var bid_type = checkOddEven(data.win_card_seq_index);

        if (bid_type == OUT && data.card_seq_index == data.win_card_seq_index) {
            // user won,
            // make a query fetch bid coins for all user & return them,
            // special case if rows[i].u_id == lead_u_id return double coin
            // save all this in database

            // make a query fetch bid coins for all user & return them,
            connection.query('SELECT * FROM round_details where r_id = ' + data.r_id, function(err, rows) {
                if (err) throw err;
                if (rows.length > 0) {
                    for (i = 0; i < rows.length; i++) {
                        if (rows[i].u_id == data.lead_u_id) {
                            var plusUserBalanceBy = (2(rows[i].bid_x * rows[i].bid_coin)); // user won
                            rows[i].won_coin = plusUserBalanceBy;
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = 0;
                        } else {
                            rows[i].won_coin = 0;
                            rows[i].return_coin = (rows[i].bid_x * rows[i].bid_coin);
                            rows[i].lost_coin = 0;
                        }
                        if ((i + 1) === rows.length) {
                            io.sockets.in(data.table_name).emit('whoWonSuccess', {
                                "data": rows
                            });
                        }
                    }
                } else {
                    io.sockets.in(data.table_name).emit('whoWonSuccess', {
                        "data": []
                    });
                }
            });

        } else {
            connection.query('SELECT * FROM round_details where r_id = ' + data.r_id, function(err, rows) {
                if (err) throw err;

                if (rows.length > 0) {
                    for (i = 0; i < rows.length; i++) {
                        if (bid_type == rows[i].in_out) {
                            // user won
                            rows[i].won_coin = (rows[i].bid_x * rows[i].bid_coin);
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = 0;
                        } else {
                            // user lost
                            rows[i].won_coin = 0;
                            rows[i].return_coin = 0;
                            rows[i].lost_coin = (rows[i].bid_x * rows[i].bid_coin);
                        }

                        if ((i + 1) === rows.length) {
                            io.sockets.in(data.table_name).emit('whoWonSuccess', {
                                "data": rows
                            });
                        }
                    }
                } else {
                    io.sockets.in(data.table_name).emit('whoWonSuccess', {
                        "data": []
                    });
                }
            });
            //io.sockets.in(data.table_name).emit('whoWonSuccess', { "start" : 'yes' });          
        }

    });

    // Start anmation success
    socket.on('startAnimation', function(data) {
        io.sockets.in(data.table_name).emit('startAnimationSuccess', {
            "start": 'yes'
        });
    });


    // get Active isers data in game table
    socket.on('activeUsers', function(data) {
        var userArray = [];
        connection.query('SELECT * FROM game_table_detail where t_id = ' + data.table_id, function(err, tdrows) {
            if (tdrows.length > 0) {
                for (i = 0; i < tdrows.length; i++) {
                    userArray[i] = tdrows[i].u_id;
                    if ((i + 1) == tdrows.length) {
                        connection.query('SELECT * FROM user where u_id IN(?)', userArray, function(err, rows) {
                            io.sockets.in(data.id).emit('successActiveUser', {
                                "userData": rows
                            });
                        });
                    }
                }
            } else {
                io.sockets.in(data.id).emit('successActiveUser', {
                    "userData": []
                });
            }
        });

    });

    socket.on('getUser', function(data) {
        connection.query('SELECT * FROM user where token = ' +"'"+ data.token+"'", function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            io.sockets.in(data.id).emit('successGetUser', {
                "userData": rows
            });
        });
    });

    // Create new Shuffle card event
    socket.on('shuffleCards', function(data) {
        shuffleObj = new shuffle();
        var desk = shuffleObj.deck();
        var cardsArray = shuffleObj.shuffleArray(desk);
        //socket.room = data.table_name;
        map.set(data.table_name, cardsArray);
        io.sockets.in(data.table_name).emit('successShuffle', {
            "shuffleCards": cardsArray
        });
    });


    socket.on('bidSelection', function(data) {
        var win_cardseq_index = '';
        var firstWin_cardseq_index = '';
        console.log(data.table_name);
        console.log(map.get(data.table_name).length);
        console.log(map.get(data.table_name));

        var table = map.get(data.table_name);
		 
		
        if (data.round_id == '') { // For the first time when round is started
            for (i = 0; i < table.length; i++) {
                if (table[i].value == data.selected_card_index) {
                    if (win_cardseq_index == '') {
                        win_cardseq_index += i;
                        firstWin_cardseq_index = i;
                    } else {
                        win_cardseq_index += "," + i;
                    }					
                }
            }
			
            connection.query('INSERT INTO round SET ?', {
                t_id: data.table_id,
                card_sequence: JSON.stringify(data.card_sequence),
                selected_card_index: data.selected_card_index,
                lead_u_id: data.u_id,
                win_cardseq_index: win_cardseq_index
            }, function(err, res) {
                if (err) throw err;				
                round_id = res.insertId;
				// set the winCardIndex to the winCardSeqIndex dictionary 				
				
				winCardSeqIndex.set(round_id,win_cardseq_index);
                connection.query('INSERT INTO round_details SET ?', {
                    u_id: data.u_id,
                    r_id: round_id,
                    bid_coin: data.bid_coin,
                    bid_x: data.bid_x,
                    total_bid: data.total_bid,
                    in_out: data.in_out
                }, function(err, rdes) {
                    if (err) throw err;
                    round_detail_id = rdes.insertId;
                    console.log('Data position ' + data.position);
                    connection.query('SELECT gtd.u_id FROM game_table_detail gtd where gtd.position = ' + (data.position + 1) + ' and t_id = ' + data.table_id, function(err, prows) {
                        if (err) throw err;
                        if (prows.length > 0) {
                            io.sockets.in(data.table_name).emit('bidSelectionSuccess', {
                                "round_id": round_id,
                                'round_detail_id': round_detail_id,
                                'u_id': data.u_id,
                                'bid_coin': data.bid_coin,
                                'bid_x': data.bid_x,
                                'total_bid': data.total_bid,
                                'in_out': data.in_out,
                                'lead_u_id': data.lead_u_id,
                                'next_bid_uid': prows.u_id,
                                'animate': 'no',
                                'win_cardseq_index': win_cardseq_index,
                                "timestamp": getTimestamp()
                            });

                        } else {
                            io.sockets.in(data.table_name).emit('bidSelectionSuccess', {
                                "round_id": round_id,
                                'round_detail_id': round_detail_id,
                                'u_id': data.u_id,
                                'bid_coin': data.bid_coin,
                                'bid_x': data.bid_x,
                                'total_bid': data.total_bid,
                                'in_out': data.in_out,
                                'lead_u_id': data.lead_u_id,
                                'next_bid_uid': '-1',
                                'animate': 'yes',
                                'win_cardseq_index': win_cardseq_index,
                                "timestamp": getTimestamp()
                            });
							
							var listSeq = winCardSeqIndex.get(round_id);
							if(listSeq != undefined){
								var array = listSeq.split(",");							
								data.win_card_seq_index = array.shift();
								var isRoundEnd = "no";
								if(array.length == 0){ // this was the last card show for this round
									isRoundEnd = yes;								
									winCardSeqIndex.delete(round_id);	
								}else{
									winCardSeqIndex.set(round_id,array.toString());	
								}
								
								data.r_id  = round_id;
								data.isRoundEnd = isRoundEnd;
								checkWhoWonRound(data);
							}else{
								// i really dont know what to do now?
							}
							
                            //io.sockets.in(data.id).emit('whoWon', data);
                        }
                        //io.sockets.in(data.table_name).emit('',{});
                    });
                });
            });
        } else {
            // for the once who are present on the table
            connection.query('INSERT INTO round_details SET ?', {
                u_id: data.u_id,
                r_id: data.round_id,
                bid_coin: data.bid_coin,
                bid_x: data.bid_x,
                total_bid: data.total_bid,
                in_out: data.in_out
            }, function(err, rdes) {
                if (err) throw err;
                round_detail_id = rdes.insertId;
                console.log('Data position ' + data.position);
				//winCardSeqIndex
                connection.query('SELECT gtd.u_id,r.win_cardseq_index FROM game_table_detail gtd LEFT JOIN round r ON r.t_id = gtd.t_id  where  r.r_id = ' + data.round_id + ' and r.t_id = ' + data.table_id + ' and gtd.position = ' + (data.position + 1) + ' and gtd.t_id = ' + data.table_id, function(err, prows) {
                    if (err) throw err;
                    if (prows.length > 0) {
                        io.sockets.in(data.table_name).emit('bidSelectionSuccess', {
                            "round_id": data.round_id,
                            'round_detail_id': round_detail_id,
                            'u_id': data.u_id,
                            'bid_coin': data.bid_coin,
                            'bid_x': data.bid_x,
                            'total_bid': data.total_bid,
                            'in_out': data.in_out,
                            'lead_u_id': data.lead_u_id,
                            'next_bid_uid': prows[0].u_id,
                            'animate': 'no',
                            'win_cardseq_index': data.win_cardseq_index,
                            "timestamp": getTimestamp()
                        });

                    } else {
                        io.sockets.in(data.table_name).emit('bidSelectionSuccess', {
                            "round_id": data.round_id,
                            'round_detail_id': round_detail_id,
                            'u_id': data.u_id,
                            'bid_coin': data.bid_coin,
                            'bid_x': data.bid_x,
                            'total_bid': data.total_bid,
                            'in_out': data.in_out,
                            'lead_u_id': data.lead_u_id,
                            'next_bid_uid': '-1',
                            'animate': 'yes',
                            'win_cardseq_index': data.win_cardseq_index,
                            "timestamp": getTimestamp()
                        });
						
						
						var listSeq = winCardSeqIndex.get(round_id);
							if(listSeq != undefined){
								var array = listSeq.split(",");							
								data.win_card_seq_index = array.shift();
								var isRoundEnd = 'no';
								if(array.length == 0){ // this was the last card show for this round
									isRoundEnd = 'yes';								
									winCardSeqIndex.delete(round_id);	
								}else{
									winCardSeqIndex.set(round_id,array.toString());	
								}
								
								data.r_id  = round_id;
								data.isRoundEnd = isRoundEnd;
								checkWhoWonRound(data);
							}else{
								// i really dont know what to do now?
							}												
                    }
                });
            });
        }
    });

    // Create new game event
    socket.on('newGame', function(data) {
        // Create new game 
        connection.query('INSERT INTO game SET ?', {
            u_id: data.u_id,
            g_dt: getTimestamp()
        }, function(err, res) {
            if (err) throw err;
            game_id = res.insertId;
            table_name_db = game_id + generateTableName();
            socket.room = table_name_db;
            socket.join(table_name_db);
            // Create new game table
            connection.query('INSERT INTO game_table SET ?', {
                g_id: game_id,
                table_name: table_name_db,
                g_dt: getTimestamp()
            }, function(err, tres) {
                if (err) throw err;
                game_table_id = tres.insertId;
                // Create game table detail entry 
                connection.query('INSERT INTO game_table_detail SET ?', {
                    g_id: game_id,
                    t_id: game_table_id,
                    u_id: data.u_id,
                    position: 0,
                    i_date: getTimestamp()
                }, function(err, tdres) {
                    if (err) throw err;
                    game_table_detail_id = tdres.insertId;
                    io.sockets.in(data.id).emit('successGame', {
                        "game_id": game_id,
                        'table_id': game_table_id,
                        'table_detail_id': game_table_detail_id,
                        'table_name': table_name_db,
                        'bidTimeOut': bidTimeOut,
                        'position': 0,
                        'shuffleCards': shuffleCard(table_name_db)
                    });
                });
            });
        });
    });
});

/*
process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err}`);
});
*/

/*	
	process.env.TZ = 'Asia/Kolkata';
	console.log (new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''));
*/