-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2016 at 01:12 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bolpatti`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `g_id` int(30) NOT NULL,
  `u_id` int(10) NOT NULL,
  `g_status` char(3) NOT NULL DEFAULT 'ON',
  `g_dt` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`g_id`, `u_id`, `g_status`, `g_dt`) VALUES
(1, 1, 'ON', '1455047409202'),
(2, 2, 'ON', '1455130877287'),
(3, 2, 'ON', '1455211817618'),
(4, 2, 'ON', '1455214825333'),
(5, 2, 'ON', '1455214969720');

-- --------------------------------------------------------

--
-- Table structure for table `game_table`
--

CREATE TABLE IF NOT EXISTS `game_table` (
  `t_id` int(30) NOT NULL,
  `g_id` int(30) NOT NULL,
  `table_name` varchar(10) DEFAULT NULL,
  `is_running` varchar(1) NOT NULL DEFAULT 'Y',
  `is_active` varchar(1) NOT NULL DEFAULT 'Y',
  `g_dt` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_table`
--

INSERT INTO `game_table` (`t_id`, `g_id`, `table_name`, `is_running`, `is_active`, `g_dt`) VALUES
(1, 1, '1LndJf', 'Y', 'Y', '1455047409211'),
(2, 2, '272TIP', 'Y', 'Y', '1455130877304'),
(3, 3, '3pG5G8', 'Y', 'Y', '1455211817626'),
(4, 4, '4vbjEU', 'Y', 'Y', '1455214825342'),
(5, 5, '5n4y0O', 'Y', 'Y', '1455214969729');

-- --------------------------------------------------------

--
-- Table structure for table `game_table_detail`
--

CREATE TABLE IF NOT EXISTS `game_table_detail` (
  `g_t_d_id` int(30) NOT NULL,
  `g_id` int(30) NOT NULL,
  `t_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `i_date` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_table_detail`
--

INSERT INTO `game_table_detail` (`g_t_d_id`, `g_id`, `t_id`, `u_id`, `status`, `i_date`) VALUES
(1, 1, 1, 1, 1, '1455047409218'),
(2, 1, 1, 2, 1, '1455047461057'),
(3, 1, 1, 2, 1, '1455047760558'),
(4, 1, 1, 2, 1, '1455048184134'),
(5, 1, 1, 2, 1, '1455130085428'),
(6, 2, 2, 2, 1, '1455130877308'),
(7, 3, 3, 2, 1, '1455211817639'),
(8, 4, 4, 2, 1, '1455214825348'),
(9, 5, 5, 2, 1, '1455214969733');

-- --------------------------------------------------------

--
-- Table structure for table `gift`
--

CREATE TABLE IF NOT EXISTS `gift` (
  `gift_id` int(30) NOT NULL,
  `gift_category` varchar(255) NOT NULL,
  `gift_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gift_details`
--

CREATE TABLE IF NOT EXISTS `gift_details` (
  `gift_d_id` int(30) NOT NULL,
  `gift_id` int(30) NOT NULL,
  `gift_d_name` varchar(255) NOT NULL,
  `gift_d_coins` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `round`
--

CREATE TABLE IF NOT EXISTS `round` (
  `r_id` int(30) NOT NULL,
  `t_id` int(30) NOT NULL,
  `u_won_id` int(30) DEFAULT NULL,
  `card_sequence` varchar(255) DEFAULT NULL,
  `selected_card_index` varchar(4) DEFAULT NULL,
  `lead_u_id` int(30) DEFAULT NULL,
  `win_cardseq_index` varchar(15) DEFAULT NULL,
  `win_index` int(1) DEFAULT '-1',
  `is_active` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `round`
--

INSERT INTO `round` (`r_id`, `t_id`, `u_won_id`, `card_sequence`, `selected_card_index`, `lead_u_id`, `win_cardseq_index`, `win_index`, `is_active`) VALUES
(1, 2, NULL, '"[{\\"value\\":1,\\"name\\":\\"1\\",\\"suit\\":\\"Diamonds\\"},{\\"value\\":8,\\"name\\":\\"8\\",\\"suit\\":\\"Diamonds\\"},{\\"value\\":2,\\"name\\":\\"2\\",\\"suit\\":\\"Hearts\\"},{\\"value\\":10,\\"name\\":\\"10\\",\\"suit\\":\\"Diamonds\\"},{\\"value\\":7,\\"name\\":\\"7\\",\\"suit\\":\\"Spades\\"},', '1', 2, '0,17,18,35', NULL, '0'),
(2, 3, NULL, '"[{\\"value\\":3,\\"name\\":\\"3\\",\\"suit\\":\\"Spades\\"},{\\"value\\":6,\\"name\\":\\"6\\",\\"suit\\":\\"Spades\\"},{\\"value\\":11,\\"name\\":\\"J\\",\\"suit\\":\\"Hearts\\"},{\\"value\\":5,\\"name\\":\\"5\\",\\"suit\\":\\"Diamonds\\"},{\\"value\\":10,\\"name\\":\\"10\\",\\"suit\\":\\"Diamonds\\"},{', '2', 2, '7,33,39,48', NULL, '0'),
(3, 4, NULL, '"[{\\"value\\":12,\\"name\\":\\"Q\\",\\"suit\\":\\"Hearts\\"},{\\"value\\":13,\\"name\\":\\"K\\",\\"suit\\":\\"Hearts\\"},{\\"value\\":7,\\"name\\":\\"7\\",\\"suit\\":\\"Clubs\\"},{\\"value\\":13,\\"name\\":\\"K\\",\\"suit\\":\\"Clubs\\"},{\\"value\\":3,\\"name\\":\\"3\\",\\"suit\\":\\"Clubs\\"},{\\"value', '2', 2, '7,8,18,40', NULL, '0'),
(4, 5, NULL, '"[{\\"value\\":13,\\"name\\":\\"K\\",\\"suit\\":\\"Hearts\\"},{\\"value\\":11,\\"name\\":\\"J\\",\\"suit\\":\\"Clubs\\"},{\\"value\\":3,\\"name\\":\\"3\\",\\"suit\\":\\"Clubs\\"},{\\"value\\":3,\\"name\\":\\"3\\",\\"suit\\":\\"Diamonds\\"},{\\"value\\":9,\\"name\\":\\"9\\",\\"suit\\":\\"Spades\\"},{\\"val', '1', 2, '5,7,19,30', NULL, '0'),
(5, 2, NULL, '"[{"value":1,"name":"1","suit":"Diamonds"},{"value":8,"name":"8","suit":"Diamonds"},{"value":2,"name":"2","suit":"Hearts"},{"value":10,"name":"10","suit":"Diamonds"},{"value":7,"name":"7","suit":"Spades"},', '1', 2, '0,17,18,35', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `round_details`
--

CREATE TABLE IF NOT EXISTS `round_details` (
  `rd_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `r_id` int(30) NOT NULL,
  `bid_coin` int(4) NOT NULL,
  `bid_x` int(1) NOT NULL,
  `total_bid` bigint(100) NOT NULL,
  `in_out` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `round_details`
--

INSERT INTO `round_details` (`rd_id`, `u_id`, `r_id`, `bid_coin`, `bid_x`, `total_bid`, `in_out`) VALUES
(1, 2, 1, 500, 1, 500, 1),
(2, 2, 2, 200, 2, 400, 1),
(3, 2, 4, 1000, 1, 1000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(30) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(50) DEFAULT NULL,
  `u_gender` varchar(6) DEFAULT NULL,
  `fb_id` varchar(50) DEFAULT NULL,
  `fb_mobile` varchar(50) DEFAULT NULL,
  `is_guest` varchar(4) DEFAULT NULL,
  `pushtoken` varchar(255) DEFAULT NULL,
  `deviec_type` varchar(10) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `balance` bigint(100) DEFAULT '10000',
  `reg_dt` varchar(40) NOT NULL,
  `last_login` varchar(40) DEFAULT NULL,
  `chk_last_login` varchar(40) DEFAULT NULL,
  `is_loggedIn` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_name`, `u_email`, `u_gender`, `fb_id`, `fb_mobile`, `is_guest`, `pushtoken`, `deviec_type`, `token`, `balance`, `reg_dt`, `last_login`, `chk_last_login`, `is_loggedIn`) VALUES
(1, 'iiiii', '', '0', '', '9898989898', '1', '5465464', 'Android', '353627071660032', 10000, '1455047407142', '1455047407142', '1455047407142', 1),
(2, 'qqqqq', '', '0', '', '9898989898', '1', '5465464', 'Android', '000000000000000', 11000, '1455047447458', '1455214968393', '1455211806011', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_purchase`
--

CREATE TABLE IF NOT EXISTS `user_purchase` (
  `up_id` int(30) NOT NULL,
  `u_id` int(30) NOT NULL,
  `u_pur_token` varchar(255) NOT NULL,
  `u_dt` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`g_id`);

--
-- Indexes for table `game_table`
--
ALTER TABLE `game_table`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `game_table_detail`
--
ALTER TABLE `game_table_detail`
  ADD PRIMARY KEY (`g_t_d_id`);

--
-- Indexes for table `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`gift_id`);

--
-- Indexes for table `gift_details`
--
ALTER TABLE `gift_details`
  ADD PRIMARY KEY (`gift_d_id`);

--
-- Indexes for table `round`
--
ALTER TABLE `round`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `round_details`
--
ALTER TABLE `round_details`
  ADD PRIMARY KEY (`rd_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `user_purchase`
--
ALTER TABLE `user_purchase`
  ADD PRIMARY KEY (`up_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `g_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `game_table`
--
ALTER TABLE `game_table`
  MODIFY `t_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `game_table_detail`
--
ALTER TABLE `game_table_detail`
  MODIFY `g_t_d_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gift`
--
ALTER TABLE `gift`
  MODIFY `gift_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gift_details`
--
ALTER TABLE `gift_details`
  MODIFY `gift_d_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `round`
--
ALTER TABLE `round`
  MODIFY `r_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `round_details`
--
ALTER TABLE `round_details`
  MODIFY `rd_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_purchase`
--
ALTER TABLE `user_purchase`
  MODIFY `up_id` int(30) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
