// Develope by Nishit Maheta

var shuffle  = function (){
var self = this;
 
 self.card  = function (index,value, name, suit) {
	 this.index = index;
     this.value = value;
  	 this.name = name;
     this.suit = suit;
  }
  
  self.deck =  function () {
				this.names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
				this.nameindex = [1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49];
				this.suits = ['Diamonds','Clubs','Hearts','Spades'];
				this.suitindex = [0,1,2,3];
				var cards = [];
				
				for( var s = 0; s < this.suits.length; s++ ) {
					for( var n = 0; n < this.names.length; n++ ) {
						
						cards.push( new self.card((this.nameindex[n]+this.suitindex[s]), n+1, this.names[n], this.suits[s] ) );
					}
				}
			
				return cards;
  }
  
 self.shuffleArray = function(o){
  	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  }
  
};

module.exports = shuffle;